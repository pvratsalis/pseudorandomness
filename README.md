# README

This is small application that allows visitors to provide their email and
get daily random emails.

In order to setup the application, the dependencies are:

* Bundler
* Bower (for assets management)

## Steps to get the application up and running

1. Get the source
2. Install the dependencies via bundle
3. Run bower install to get boostrap and any other dependencies mentioned in the bower.json file
4. Install fortune command line program on the server machine (if another source
of random messages is used, this is not needed)
5. Run the rake db:create and rake db:migrate to get the database ready for the application
6. Check the application: rails s
7. To send the daily message email, there is a rake task message:daily

** Note: in order to get/send emails, in the development environment you can
create a .env file replicating the contents of the .env-example file but with
actual values. For production, dotenv is not running by default.

## Functionality
A visitor can enter his/her email and be subscribed to the list of subscribers.
Since this could be exploited for spamming the users, a double opt-in mechanism
is in place, so the user needs to verify his/her email address via a verification
link he/she receives by email. Then, the user will start receiving emails with
pseudorandom content.

The current functionality depends on the fortune command line program to get its
random messages. Nevertheless, it's easy to use another source, as long as you
implement a new MessageBuilder subclass, that needs to somehow create the messages
and get the html and text forms of them (implementing the build, to_html, to_text
methods).

Ideas about other sources for the messages would be:

* Getting them from an API endpoint (either internal or a public service)
* Getting them from a file
* Getting them from a database


## Notes

The Rake task's implementation is to get the verified users and send them emails
one by one. Another approach (which would be better in my view), would be to use
a newsletter creation software (eg MailChimp) and on subscription, add the users
via an API call to the mailchimp API in a list (it can happen on user creation
or just on user email verification). Then, the newsletter could be triggered with
the proper content in the MailChimp template on a daily basis. This would be
especially suitable if we had several users. In my view those emails are not
transactional, so it is not justified to send them via mandrillapp for example,
I would consider them marketing emails.

In that case, an unsubscribe functionality would be good, too, in order to allow
users to stop receiving those emails.