#
# Validates a string is of a valid email format.
#
class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    pattern = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
    unless value =~ pattern
      record.errors[attribute] << (options[:message] || "is not an email")
    end
  end
end
