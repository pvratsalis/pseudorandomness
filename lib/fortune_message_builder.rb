#
# Uses (and assumes it's installed) the fortune command line program
# to get pseudorandom messages/quotes/riddles and format them as html or text
#
class FortuneMessageBuilder < MessageBuilder
  # allow setting the message directly
  attr_writer :message

  #
  # Build the message
  #
  def build
    @message = `fortune`
  end

  #
  # Transform the message into html
  #
  def to_html
    return @html_message if @html_message

    @html_message = @message.gsub(/\n/, '<br />')
    @html_message.gsub!(/\t/, '&nbsp;' * 4)

    @html_message
  end

  #
  # Transform the message into plain text
  #
  def to_text
    @message
  end
end
