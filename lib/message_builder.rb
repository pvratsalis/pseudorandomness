#
# A common interface for MessageBuilders to be used by other implementors
#
class MessageBuilder
  def build
    raise NotImplementedError
  end

  def to_html
    raise NotImplementedError
  end

  def to_text
    raise NotImplementedError
  end
end
