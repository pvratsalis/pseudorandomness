namespace :message do
  desc "Send the daily email messages to our subscribed users"
  task daily: :environment do
    verified_users = User.only_verified

    # get a message builder for html and text messages
    message_builder = FortuneMessageBuilder.new

    # build the message
    message_builder.build

    # get the different message formats
    html_message = message_builder.to_html
    text_message = message_builder.to_text

    verified_users.each do |user|
      UserMailer.daily_message_email(user, html_message, text_message).deliver_later
    end
  end
end
