FactoryGirl.define do
  factory :user do
    email Faker::Internet.email
    verified 0

    trait :verified do
      verified 1
    end
  end
end
