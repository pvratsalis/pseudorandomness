require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { FactoryGirl.create(:user) }

  describe "Validations" do
    it "should have a valid email as input" do
      expect(user.errors).to be_empty

      user.email = 'invalid'
      user.validate
      expect(user.errors).not_to be_empty
    end

    it "should have a unique email" do
      expect(user).to validate_uniqueness_of(:email)
    end

    it "should have a valid verified attribute" do
      user.verified = 1
      expect(user.validate).to be_truthy

      expect { user.verified = 2 }.to raise_error(ArgumentError)
    end
  end

  it "should create tokens on user creation" do
    user = FactoryGirl.build(:user)
    expect(user.verification_token).to be_nil
    user.save

    expect(user.verification_token.length).to be(32)
  end

  it "#verify" do
    expect(user.verified?).to be_falsy
    user.verify

    expect(user.verified?).to be_truthy
    expect(user.verification_token).to be_nil
  end

  it ".find_by_it_and_verification_token" do
    result = User.find_by_id_and_verification_token(id: user.id, token: user.verification_token)
    expect(result.id).to be(user.id)
  end
end
