require 'rails_helper'

describe "User email verification", type: :feature do
  it "verifies unverified user and token is discarded then" do
    user = FactoryGirl.create(:user)
    visit "/users/#{user.id}/verify?verification_token=#{user.verification_token}"
    expect(page).to have_css('.alert-success')

    # token should be nilified now
    visit "/users/#{user.id}/verify?verification_token=#{user.verification_token}"
    expect(page).to have_css('.alert-danger')
  end

  it "does not verify invalid token id combination" do
    user = FactoryGirl.create(:user)
    visit "/users/#{user.id}/verify?verification_token=invalid"
    expect(page).to have_css('.alert-danger')
  end
end
