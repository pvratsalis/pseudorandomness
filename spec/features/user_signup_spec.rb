require 'rails_helper'

describe "User/subscription creation", type: :feature do
  it "shows validation errors if invalid email is provided" do
    visit "/users/new"
    within '.new_user' do
      fill_in 'user[email]', with: 'invalid'
    end
    click_button 'Subscribe!'
    expect(page).to have_css('.alert-danger')
  end

  it "passes and creates user if valid email is provided" do
    visit "/users/new"
    within '.new_user' do
      fill_in 'user[email]', with: 'pvratsalis@gmail.com'
    end
    click_button 'Subscribe!'

    # the success alert is displayed
    expect(page).to have_css('.alert-success')

    # an email is sent for verification
    last_mail = ActionMailer::Base.deliveries.last
    expect(last_mail.to).to eq(['pvratsalis@gmail.com'])
    expect(last_mail.subject).to have_content('Verify your email address')

    # the user has been created
    expect(User.find_by(email: 'pvratsalis@gmail.com')).not_to be_nil
  end
end
