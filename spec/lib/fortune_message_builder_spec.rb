require 'spec_helper'

RSpec.describe FortuneMessageBuilder do
  let(:builder) { FortuneMessageBuilder.new }

  it "should create proper html format from message" do
      builder.message = "the message\n is this\t"
      expect(builder.to_html).to eq('the message<br /> is this' + '&nbsp;'*4)
  end

  it "should create proper text format from message" do
    message = builder.message = "the message\n is this\t"
    expect(builder.to_text).to eq(message)
  end
end
