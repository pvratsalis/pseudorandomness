class UsersController < ApplicationController
  #
  # This is the main page where a visitor can subscribe for the daily email
  #
  def new
    @user = User.new
  end

  #
  # Handle the subscription (show validation errors or actually subscribe
  # the visitor and send verification email)
  #
  def create
    @user = User.new(user_params)

    if @user.save
      flash[:success] = 'You have successfully subscribed! Verify your email (check your inbox) and you are done!'
      UserMailer.verification_email(@user).deliver_later
      redirect_to root_url
    else
      render :new
    end
  end

  #
  # Verify the user after subscription
  #
  def verify
    @user = User.find_by_id_and_verification_token(id: params[:id], token: params[:verification_token])

    @invalid_token = true
    if @user
      @invalid_token = false
      @user.verify
      @user.save
    end
  end

  private
  def user_params
    params.require(:user).permit(:email)
  end
end
