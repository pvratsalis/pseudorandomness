#
# Holds the emails of people registered for the daily email
#
class User < ActiveRecord::Base
  # Is the user's email verified (double opt-in)
  enum verified: [:unverified, :verified]

  # email should be unique in the database and be a valid email
  validates :email, uniqueness: true, email: true

  before_create :build_tokens
  before_save :normalize_email

  # get only the verified users
  scope :only_verified, -> { where(verified: 1) }

  #
  # Verifying a user means to mark him/her as verified (in order to start
  # getting emails) and destroy the verification token, as it's no longer
  # needed
  #
  def verify
    verified!
    self.verification_token = nil
  end

  class << self
    def find_by_id_and_verification_token(params)
      id, token = params[:id], params[:token]
      self.find_by(id: id, verification_token: token)
    end
  end

  private
  #
  # Creates the verification token for the user
  #
  def build_tokens
    self.verification_token = build_token
  end

  #
  # Generic method to build and return a random token in order to be used
  # by more specific methods (to build verification/unsubscribe etc tokens)
  #
  def build_token
    SecureRandom.base64(24)
  end

  #
  # Normalize the email before storing in the database
  #
  def normalize_email
    self.email = self.email.downcase
  end
end
