#
# Holds some common functionality and settings for all mailers
#
class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@pseudorandomness.com'
end
