class UserMailer < ApplicationMailer
  #
  # The email sent to user just subscribed in order to verify his/her email address
  #
  def verification_email(user)
    @user = user
    @url = verify_user_url(id: user.id, verification_token: user.verification_token)

    mail(to: user.email, subject: 'Verify your email address')
  end

  #
  # The email sent daily to users
  #
  def daily_message_email(user, html_message, text_message)
    @user = user
    @html_message = html_message
    @text_message = text_message

    mail(to: user.email, subject: 'Your daily shot of pseudorandomness')
  end
end
