class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :verification_token
      t.string :unsubscribe_token
      t.integer :verified, default: 0
      t.timestamps null: false
    end
  end
end
